package br.com.tcc.leilao.database.contrato;

import android.provider.BaseColumns;

public interface UsuarioContrato extends BaseColumns {

    String TABELA_NOME = "usuarios";
    String CHAVE_NOME = "nome";

}
