package br.com.tcc.leilao.integrationTest;

import java.util.List;

import br.com.tcc.leilao.api.retrofit.client.LeilaoWebClient;
import br.com.tcc.leilao.api.retrofit.client.RespostaListener;
import br.com.tcc.leilao.unitTest.Leilao;
import br.com.tcc.leilao.integrationTest.recyclerview.adapter.ListaLeilaoAdapter;


public class AtualizadorDeLeiloes {


    public void buscaLeiloes(final ListaLeilaoAdapter adapter, LeilaoWebClient client, final ErrorListener listener) {
        client.todos(new RespostaListener<List<Leilao>>() {
            @Override
            public void sucesso(List<Leilao> leiloes) {
                adapter.atualiza(leiloes);
            }

            @Override
            public void falha(String mensagem) {
                listener.exibirMensagemErro(mensagem);
            }
        });
    }

    public interface ErrorListener {
        void exibirMensagemErro(String mensagem);
    }


}

